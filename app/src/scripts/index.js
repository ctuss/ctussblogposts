import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'

/**
 * Global Constants
 */
const postUrl = "https://jsonplaceholder.typicode.com/posts/";
const userUrl = "https://jsonplaceholder.typicode.com/users/";

/**
 * Variables
 */
let posts = [];

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */

class Post {
    constructor(id, title, userId, body, photo) {
        this.id = id;
        this.title = title,
            this.userId = userId;
        this.body = body;
        this.photo = photo;
        posts.push(this);
    }
    /*get title() {
        return this.title;
    }
    get body() {
        return this.body;
    }
    get id() {
        return this.id;
    }*/
}
/*let post = {};
function post(id, title, userId, body, photo) {
    this.id = id;
    this.title = title,
    this.userId = userId;
    this.body = body;
    this.photo = photo;
}*/
$(document).ready(function () {
    const xhr = $.ajax({
        url: `${postUrl}`,
        method: 'GET',
        success: function (data) {
            const $app = $('#app');
            $(data).each((i, post) => {
                const elPost = `
                <li id = "${post.id}" class ="list-group-item" margin: 0; padding: 0; width:100px; >
                 <div class="mask flex-center rgba-red-strong">
                 <p>${post.title}</p></div>
                </li>
                `;
                $(elPost).appendTo('.list-group-flush');
                $(`#${post.id}`).click(function (e) {
                    $('.list-group-item').hide();
                    createCard(post.photo, post.title, post.body, post.id);
                    //const thePost = createCard(post.photo, post.title, post.body, post.id );
                    //app.append(thePost);
                    const button = `<button id="knapp" class="btn btn-info">Go back</button>`;
                    $(button).appendTo('.inner');
                    $(`#knapp`).click(function (e) {
                        $('#knapp').remove();
                        $('.col-4').last().remove();
                        $('.list-group-item').show();
                    });
                });
                //const thePost = createCard(post.photo, post.title, post.body );
                const newPost = new Post(post.id, post.title, post.userId, post.body, post.photo);

                //$app.append(thePost);
            })

        }
    });

    //fetchPosts();
    // TODO Add your listeners here
});
/**
 * Build a Card template with a picture.
 * @param {string|null} photo
 * @param {string} title
 * @param {string} body
 * @returns String
 */
async function createCard(url, title, body, id) {
    let idUser = 0;
    let userName = '';
    let userUsername = '';
    let userEmail = '';
    let userWebsite = '';
    posts.forEach(post => {
        if (post.id == id) {
            idUser = post.userId;
            console.log(post.userId);
            console.log(post.id);
            //idUser = post.userId;
            console.log(idUser);
        }

    });
    $.ajax({
        url: `${userUrl}${idUser}`,
        method: 'GET',
        success: function (e) {
            $(e).each((i, user) => {
                userName = e.name;
                userUsername = e.username;
                userEmail = e.email;
                userWebsite = e.website;
                console.log(e);
                console.log(userName);
            })
        },

    }).done(function (e) {
        console.log(userName);
        console.log(userUsername);
        console.log(userEmail);
        console.log(userWebsite);
        postCard(url, title, body, userName, userEmail, userWebsite);



    });
    //postCard(url, title, body, userName,userEmail, userWebsite);
}
/**
 * Build a Card template with a picture.
 * @param {string|null} url
 * @param {string} title
 * @param {string} body
 * @param {string} userName
 * @param {string} email
 * @param {string} website
 * @returns String
 */
function postCard(url, title, body, userName, email, website) {

    const $app = $('#app');
    $app.append(`
        <div class="col-4">
            <div class="card h-100" style="18rem;">
                ${url
            ? `
                        <div class="card-img">
                            <img class="card-img-top" src="${url}"/>
                        </div>
                      ` : ''}
                <div class="card-body">
                    <h5>${title}</h5>
                    ${body.split('\n').map(para => `<p>${para}</p>`).join('\n')}
                    <p> user name: ${userName} </p>
                    <p> user email: ${email} </p>
                    <p> user website: ${website} </p>
                </div>
            </div>
            
        </div>
    `);
}

